const canvas = document.querySelector("canvas");

// set width and height of the canvas
const width = window.innerWidth;
const height = window.innerHeight;

// set height and width of the screen
canvas.width = width;
canvas.height = height;

// call get context to draw 2d shape
const ctx = canvas.getContext("2d");

class Ball {
  constructor(x, y, velx, vely, size, color) {
    this.x = x; //horizontal position of ball
    this.y = y; //vertical position of ball
    this.velx = velx; //velocity x added to co-ordinate x
    this.vely = vely; // velocity y added to co-ordinate y
    this.size = size; //size is the radius of the ball
    this.color = color; //fill ball shape with given color
  }

  // create draw function
  drawBall() {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.fill();
  }

  // create an update function

  updateBall() {
    // if x and y position is greater or less than browser viewport
    // the ball turns to other diretion.
    if (this.x + this.size >= width || this.x - this.size <= 0) {
      this.velx = -this.velx;
    }

    if (this.y + this.size >= height || this.y - this.size <= 0) {
      this.vely = -this.vely;
    }

    // x and y velocity added to x and y coordinate everytime the updateBall
    // function is called

    this.x += this.velx;
    this.y += this.vely;
  }
}

// create random number generator function

function random(min, max) {
  const num = Math.floor(Math.random() * (max - min + 1)) + min;
  return num;
}

// create some balls and store in array

const balls = [];

while (balls.length < 50) {
  let size = random(10, 20);

  // create a new instance of Ball
  const ball = new Ball(
    random(size, width - size),
    random(size, height - size),
    random(-5, 5),
    random(-5, 5),
    size,
    `rgb(${random(0,255)},${random(0,255)},${random(0,255)})`
  );
  balls.push(ball);
}

// create loop function

function loop() {
  // cover prev frame dreawing before newxt one is drawn
  ctx.fillStyle = "rgba(0,0,0,0.25)";
  ctx.fillRect(0, 0, width, height);

  // run necessary function
  for (let i = 0; i < balls.length; i++) {
    balls[i].drawBall();
    balls[i].updateBall();
  }

  // lets call loop function itself over and over again
  // and make animation smooth

  requestAnimationFrame(loop);
}

// finally call the loop function once it starts

loop();

// this below given is trial for another way to accomplish this task but relatively primitive


// // frames per second
// const FPS = 30;

// // get height and width of the screen
// const height = window.innerHeight;
// const width = window.innerWidth;

// canvas.width = width;
// canvas.height = height;
// // ball size
// var bs = 30;
// // co-ordinate
// var bx, by;
// // velocity
// var xv, yv;

// var context;

// // load canvas
// const canvas = document.getElementById("gameCanvas");
// context = canvas.getContext("2d");

// // set up interval
// setInterval(update, 1000 / FPS);

// // ball starting point

// bx = canvas.width / 2;
// by = canvas.height / 2;

// // random ball starting speed (between 25 and 100)

// xv = Math.floor(Math.random() * 76 + 25) / FPS;
// yv = Math.floor(Math.random() * 76 + 25) / FPS;

// // random ball direction
// if (Math.floor(Math.random() * 2) == 0) {
//   xv = -xv;
// }
// if (Math.floor(Math.random() * 2) == 0) {
//   yv = -yv;
// }

// // update function
// function update() {
//   // move the ball
//   bx += xv;
//   by += yv;

//   // bounce off the wall
//   if (bx - bs / 2 < 0 && xv < 0) {
//     xv = -xv;
//   }

//   if (bx - bs / 2 > canvas.width && xv > 0) {
//     xv = -xv;
//   }

//   if (by - bs / 2 < 0 && yv < 0) {
//     yv = -yv;
//   }

//   if (by - bs / 2 > canvas.width && yv > 0) {
//     yv = -yv;
//   }

//   // draw background abd ball

//   for(let i = 0; i < 10; i++){
//     context.beginPath();
//     context.arc(bx - bs / 2, by - bs / 2, bs, 0, 2 * Math.PI, false);
//     context.lineWidth = 1;
//     context.strokeStyle = "#FF0000";
//     context.stroke();
//   }

// //   context.fillRect(bx - bs / 2, by - bs / 2, bs, bs);
// }
